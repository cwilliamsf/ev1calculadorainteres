/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author camilo williams
 */
public class CalcInteres {
    
    private int Capital;
    private double Interes = 0.1;
    private int Plazo;
    private int Resultado;

    /**
     * @return the Capital
     */
    public int getCapital() {
        return Capital;
    }

    /**
     * @param Capital the Capital to set
     */
    public void setCapital(int Capital) {
        this.Capital = Capital;
    }

    /**
     * @return the Interes
     */
    public double getInteres() {
        return Interes;
    }

    /**
     * @param Interes the Interes to set
     */
    public void setInteres(double Interes) {
        this.Interes = Interes;
    }

    /**
     * @return the plazo
     */
    public int getPlazo() {
        return Plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.Plazo = Plazo;
    }

    /**
     * @return the Resultado
     */
    public int getResultado() {
        return Resultado;
    }

    /**
     * @param Resultado the Resultado to set
     */
    public void setResultado(int Resultado) {
        this.Resultado = Resultado;
    }
    
    public void cInteres(int Capital, int Plazo){
        this.Resultado = (int) (Capital * Interes * Plazo);


}
    
    
    
}
